# ReduxSimpleStarter

React web app build based on [Redux Tutorial on Udemy](https://www.udemy.com/react-redux/)?

### Getting Started

Checkout this repo, install dependencies, then start the gulp process:

```
> git clone https://gitlab.com/omdjin/redux-simple-starter.git
> npm install
> npm start
```
